import os
from flask import Flask
from config import basedir
from flask_bootstrap import Bootstrap

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret-secret-string'

# WTF_CSRF_SECRET_KEY = 'a random string'

bootstrap = Bootstrap(app)

# get html from views files
from app import views
