# throwing error- comment out until source determined
# from flask_uploads import UploadSet, TEXT, DOCUMENTS, DATA

from flask_wtf import Form
from flask_wtf.file import FileField, FileAllowed, FileRequired
from wtforms import SelectField, SelectMultipleField, StringField, \
		BooleanField, SubmitField
from wtforms.validators import Length, InputRequired

dataTypes = ['gene', 'exon', 'RPKM', 'spljxn']
class DataSetup(Form):
	upload = FileField(u'Data Set', validators=[
		FileRequired(),
		FileAllowed(['txt','rpkm'],'no clinical data here')
	])	
	options = SelectField(u'Data Set Type', choices=[(d, d) for d in dataTypes], validators=[InputRequired()])
	submit = SubmitField()
