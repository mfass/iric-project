from flask import render_template, url_for, flash, redirect
from app import app
from .forms import DataSetup

SECRET_KEY = 'asdfgjvhanjsf'

@app.route('/index', methods=('GET','POST'))
def index():
	return render_template('index.html')

@app.route('/', methods=['GET','POST'])
@app.route('/upload')
def upload():
	form = DataSetup()
	if form.validate_on_submit():
		flash('you did it')		
		return redirect(url_for('success'))
	return render_template('upload.html', form=form)

@app.route('/success', methods=['GET', 'POST'])
def success():
	return render_template('success.html')

