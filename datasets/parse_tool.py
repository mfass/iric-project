"""
Parsing tool. Takes patient data as a txt file and attaches it to each category.
Outputs the name of the category and the annotation thing to txt file, throws out 
any Non Applicaple things, lists categories that are excluded from the output at the end. 

Eventually this piece should be able to look through a file, find the patient ID
being queried, and output same stuff we're looking at here. 

@author Megan Fass
last updated May 25 2016 16h30

"""

import os
import sys
import re
import numpy as np 

# function for opening, reading, closing input file
# assumes 2 different files, may or may not be true
#	in final implementation.
 
def file_input(user_prompt):
	user_input = raw_input(user_prompt)
	data = open(user_input)
	store_data = data.read()
	data.close()
	return store_data	
 

data_categories = file_input("data category file name: ")
patient_info = file_input("patient data file name: ")

# plot twist: the split fuction creates a list type to store the results
# 	of the split. Wow. Megan ur dumb. Anyway. So we have a list called
#	category list. new list, patient data. Same process. Pass the contents
#	of both lists to a new 2D list. profit. 

category_list = re.split('\t', data_categories)
# print len(category_list) 

patient_list = re.split('\t', patient_info)
# print len(patient_list)

# for later: if len of category_list doesn't equal len(patient_list), 
#	throw some sort of Error

# in patient_data[0][0]= category_list[0]
# patient_data[0][1] = patient_list[0]

patient_data = []
for i in range(len(category_list)):
	patient_data.append([])
	patient_data[i].append(category_list[i])
	patient_data[i].append(patient_list[i])

#there's that /n at the end of the thing. we dont want that.. 
# so we need ot go in and access the strings in both indices

end = len(patient_data) - 1

cutoff_0 = len(patient_data[end][0]) - 1
patient_data[end][0] = patient_data[end][0][0:cutoff_0]

cutoff_1 = len(patient_data[end][1]) - 1
patient_data[end][1] = patient_data[end][1][0:cutoff_1]

# print patient_data
# print ""

# time to throw out everything that includes a "[Not Applicable]" string
# we know that the string will only be found in patient_data[i][1] so 
# we can hard code that
# other cases of missing data: "[Not Available]", "[Not Evaluated]"

i = 0
limit = len(patient_data)
removed_list = []
while i < limit:
	if patient_data[i][1] == "[Not Applicable]":
		removed_list.append(patient_data[i].pop(0))
		del patient_data[i]
		i -= 1
		limit -= 1
	elif patient_data[i][1] == "[Not Available]":
		removed_list.append(patient_data[i].pop(0))		
		del patient_data[i]
		i -= 1
		limit -= 1
	elif patient_data[i][1] == "[Not Evaluated]":
		removed_list.append(patient_data[i].pop(0))
		del patient_data[i]
		i -= 1
		limit -= 1
	i += 1

print ""
print patient_data
print ""
print removed_list


